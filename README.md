# Drawings

TO DO:

* Import Rihn's new cart drawings
* Climbing Wall
  * Add BOM
  * Make dimensions better
* Frame
  * Add BOM
  * Add page for all2x4 configuration 
  * Move plywood things to separate page 
  * Generally reduce the amount of stuff on one page and make the diagrams bigger
  * Show how the diagonal crossbraces connect to the frame
* Posts
  * Add BOM
  * Better way of showing where the posts go?
* Railing
  * Add BOM
  * Show In/Out for coaster tower
  * Better way of showing which railings are in/out? also move to separate page
* Stairs
  * Redo dimensions for the stringer (show the outline of the full 2x12, how to cut with framing square)
  * Drawning of riser and tread
  * How the 2x4s that support each step go in
  * Drawing of the platform at top and bottom
  * Better drawing of the diagonal railing
  * Drawings of the short railings that go around the top platform
  * Add BOM
* Coaster start platform (Create this)
  * Drawing of tower track support.SLDASM
  * Drawing of top_fence_and_interlock_SLDPRT
